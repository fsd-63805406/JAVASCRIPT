//Create a function that returns the number of hashes and pluses in a string.
//Examples
// console.log(hashPlusCount("###+") ➞ [3, 1])

// console.log(hashPlusCount("") ➞ [0, 0])

// Notes
// Return [0, 0] for an empty string.
// Return in the order of [hashes, pluses].


function hashpluscount(str){
    hashcount=0
    pluscount=0
    values=[]
    for(i=0;i<str.length;i++){
        if(str[i]=='#'){
            hashcount+=1
        }
        else if(str[i]=='+'){
            pluscount+=1
        }
    }
    counts=[hashcount,pluscount]
    return counts
    

}
console.log(hashpluscount("###+"))
// Write a JavaScript program using a for loop to calculate the sum of all integers from 1 to a given positive number n.

// Explanation:
// The loop iterates from 1 to n and accumulates the sum.

// Examples
// console.log(calculateSum(5)) ➞ 15
// console.log(calculateSum(10)) ➞ 55



function caluculatesum(number){
    sum=0
    for(i=1;i<=number;i++){
        sum+=i

    }
    return sum
}
console.log(calculatesum(5))
console.log(calculatesum(10))

// Write a JavaScript program using a for loop to find the factorial of a given positive integer n.

// Explanation:
// The loop multiplies the current value of the factorial by the loop variable.

// Examples
// console.log(calculateFactorial(5)) ➞ 120
// console.log(calculateFactorial(0)) ➞ 1
// console.log(calculateFactorial(7)) ➞ 5040


function calculateFactorial(number){
    fact=1
    for(i=1;i<=number;i++){
        fact=fact*i
    }
    return fact

}
console.log(calculateFactorial(5))
console.log(calculateFactorial(0))
console.log(calculateFactorial(7))
// Write a JavaScript program using a for loop to print a pattern of asterisks in the shape of a right-angled triangle.

// Explanation:
// The loop is used to control the number of asterisks in each row.

// Examples

// console.log(printTriangle(5)) ➞ 
// *
// * *
// * * *
// * * * *
// * * * * *

// console.log(printTriangle(2)) ➞ 

// *
// * *

function printTriangle(number) {
    let result = '';
  
    for (let i = 1; i <= number; i++) {
      for (let j = 1; j <= i; j++) {
        result += '*';
        if (j < i) {
          result += ' ';
        }
      }
  
      result += '\n';
    }
  
    return result;
  }
  
  console.log(printTriangle(5));
  
  console.log(printTriangle(2));


  // In this challenge, you must generate a sequence of consecutive numbers, from a lower bound that will always be equal to 1, up to a variable given higher bound (including the bounds in the sequence).

// Each number of the sequence that can be exactly divided by 4 must be amplified by 10 (see notes below).

// Given a higher bound num, implement a function that returns an array with the sequence of numbers, after that every multiple of 4 has been amplified.

// Examples
// console.log(amplify(4)) ➞ [1, 2, 3, 40]
// // Create a sequence from 1 to 4
// // 4 is exactly divisible by 4, so it will be 4*10 = 40

// console.log(amplify(3)) ➞ [1, 2, 3]
// // Create a sequence from 1 to 3
// // There are no numbers that can be exactly divided by 4

// console.log(amplify(25)) ➞ [1, 2, 3, 40, 5, 6, 7, 80, 9, 10, 11, 120, 13, 14, 15, 160, 17, 18, 19, 200, 21, 22, 23, 240, 25]
// // Create a sequence from 1 to 25
// // The numbers exactly divisible by 4 are: 4 (4*10 = 40), 8 (8 * 10 = 80)... and so on.


// Notes

// The given parameter num will always be equal to or greater than 1.
// Remember to include the num as the higher bound of the sequence (see the Examples) above.
// A number a amplified by a factor b can also be read as: a * b.
// A number a is exactly divisible by a number b when the remainder of the division a / b is equal to 0.
// If you get stuck on a challenge, find help in the Resources tab.
// If you're really stuck, unlock solutions in the Solutions tab.


function amplify(number) {
    let values = [];

    for (let i = 1; i <= number; i++) {
        if (i % 4 === 0) {
            values.push(i * 10);
        } else {
            values.push(i);
        }
    }

    return values;
}

console.log(amplify(4));
console.log(amplify(3));
console.log(amplify(25));
