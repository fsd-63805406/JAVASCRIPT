//Math functions
//built-in functions in javascript
console.log(Math.min(10,20));
console.log(Math.max(10,20,40,70));
let arr = [10,40,90,98]
console.log(Math.min(...arr)); //spread operator for unpacking elements
console.log('floor of 3.552---'+Math.floor(3.552));
console.log('seil of 3.552---'+Math.ceil(3.552));
console.log(Math.floor(4));
console.log(Math.floor(Math.random()*10)) //random lies between 0 and 1
console.log(Math.round(34.6))
let number = 12.876

